export CLICOLOR=1
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
alias ll='ls -alG'
export JAVA_HOME=/usr/libexec/java_home
#PS1='[\u@\h \w]$ '
export HISTSIZE=20000
export HISTTIMEFORMAT="%F %T "
export HISTCONTROL=ignoredups
export LESS="-M -x4 -R"
source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

alias vi="/usr/local/bin/vim"
alias vim="/usr/local/bin/vim"
alias tmux-ssh="tmux new-session -s pair"
source ~/.bashrc.puppet
